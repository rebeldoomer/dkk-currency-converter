import tkinter as tk
from tkinter import ttk, scrolledtext
import json

# Hardcoded conversion rates from DKK to other currencies
conversion_rates = {
    "EUR": 0.13,
    "USD": 0.15,
    "JMD": 22.49,
    "LTL": 0.38  # Note: Using historical value for example purposes
}

# Rainbow colors for the history display
rainbow_colors = ['#FF0000', '#FF7F00', '#FFFF00', '#00FF00', '#0000FF', '#4B0082', '#9400D3']

# Try to load the last used currency; default to "EUR" if not found
try:
    with open('last_currency.json', 'r') as f:
        last_currency = json.load(f)
except FileNotFoundError:
    last_currency = "EUR"

# Initialize history list
conversion_history = []

def convert_to_dkk():
    # Convert from the selected currency back to DKK
    selected_currency_to_dkk = currency_to_dkk.get()
    foreign_amount = float(foreign_amount_entry.get())
    dkk_amount = foreign_amount / conversion_rates[selected_currency_to_dkk]
    result = f"{foreign_amount} {selected_currency_to_dkk} = {dkk_amount:.2f} DKK"
    update_history(result)

def convert_from_dkk():
    # Convert from DKK to the selected currency
    dkk_amount = float(amount_entry.get())
    selected_currency_from_dkk = currency_from_dkk.get()
    foreign_amount = dkk_amount * conversion_rates[selected_currency_from_dkk]
    result = f"{dkk_amount} DKK = {foreign_amount:.2f} {selected_currency_from_dkk}"
    update_history(result)

def update_history(result):
    # Update the conversion history and display
    output_label.config(text=result)
    conversion_history.append(result)
    if len(conversion_history) > 30:
        conversion_history.pop(0)  # Keep only the last 30 entries
    # Update the history display
    history_text.config(state=tk.NORMAL)  # Enable editing of the text widget
    history_text.delete(1.0, tk.END)  # Clear existing text
    for idx, entry in enumerate(conversion_history):
        history_text.insert(tk.END, entry + "\n", str(idx))
    history_text.config(state=tk.DISABLED)  # Disable editing of the text widget
    # Apply rainbow colors
    for i, _ in enumerate(conversion_history):
        history_text.tag_config(str(i), foreground=rainbow_colors[i % len(rainbow_colors)])

app = tk.Tk()
app.title("Dansih Currency Converter")
app.configure(bg='#2B2B2B')

# DKK to X conversion
tk.Label(app, text="Amount in krowns (kr.)", fg='#00FF00', bg='#2B2B2B').pack()
amount_entry = tk.Entry(app, fg='#00FF00', bg='#3C3F41', insertbackground='white')
amount_entry.pack()
currency_from_dkk = tk.StringVar(app)
currency_from_dkk.set(last_currency)  # Use last used currency
ttk.OptionMenu(app, currency_from_dkk, last_currency, "EUR", "USD", "JMD", "LTL").pack()
tk.Button(app, text="Convert", command=convert_from_dkk, fg='#00FF00', bg='#3C3F41').pack()

# X to DKK conversion
tk.Label(app, text="Amount in Selected Currency", fg='#00FF00', bg='#2B2B2B').pack()
foreign_amount_entry = tk.Entry(app, fg='#00FF00', bg='#3C3F41', insertbackground='white')
foreign_amount_entry.pack()
currency_to_dkk = tk.StringVar(app)
currency_to_dkk.set("EUR")  # Default value
ttk.OptionMenu(app, currency_to_dkk, "EUR", "USD", "JMD", "LTL").pack()
tk.Button(app, text="Convert to krowns (kr.)", command=convert_to_dkk, fg='#00FF00', bg='#3C3F41').pack()

output_label = tk.Label(app, text="Conversion Result:", fg='#00FF00', bg='#2B2B2B')
output_label.pack()

# Integrated history display
history_text = scrolledtext.ScrolledText(app, width=40, height=10, fg='#00FF00', bg='#2B2B2B', state=tk.DISABLED)
history_text.pack()

app.mainloop()
