# Danish Currency Converter

The Danish Currency Converter is an easy-to-use desktop application designed to help users convert Danish Krone (DKK) to other currencies and vice versa. Featuring a simple interface, it supports multiple currencies and displays a colorful history of recent conversions. Convert DKK to USD, Euro, Jamaican Dollar, Litas. I was tired of how other currency converters having too many currency options that I did not need.

## Getting Started

These instructions will guide you on how to set up and run the Danish Currency Converter on your local machine.

### Prerequisites

- Python 3.x
- tkinter (usually included with Python)

### Installation

1. Make sure Python is installed on your system. If not, download and install it from [python.org](https://www.python.org/downloads/).

2. Clone the repository to your local machine:

`git clone https://gitlab.com/rebeldoomer/danish-currency-converter.git`


3. Navigate to the project directory:

`cd danish-currency-converter`




4. Run the application:

`python dkk.py`

sl


## Usage

1. To convert from DKK to another currency, enter the amount in Danish Krone in the "Amount in krowns (kr.)" field and select the target currency from the dropdown menu. Click "Convert" to see the result.

2. To convert from another currency to DKK, enter the amount in the "Amount in Selected Currency" field, select the currency from the dropdown menu, and click "Convert to krowns (kr.)" to get the result in Danish Krone.

3. The conversion result will be displayed immediately below the convert buttons, and a history of recent conversions will be shown at the bottom in a rainbow-colored text.

## Contributing

Contributions are welcome 

## License

This project is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-nc-sa/4.0/](http://creativecommons.org/licenses/by-nc-sa/4.0/) 

## Project Status

The Danish Currency Converter is currently in a stable version. 
